
package igbappcalculator.simplecalculator;

public class VerySimpleCalculator {
    public VerySimpleCalculator(){
        System.out.println("Welcome to IGB Simple Calculator...");
    }
    
    public int addNumbers(int num1, int num2){
        return num1+num2;
    }
    public int subtractNumbers(int num1, int num2){
        return num1 - num2;
    }
    public int multiplyNumbers(int num1, int num2){
        return num1 * num2;
    }
    public int divideNumbers(int num1, int num2){
        return num1 / num2;
    }
}
